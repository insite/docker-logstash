FROM docker.elastic.co/logstash/logstash:5.6.9

USER root
RUN yum -y install zip
RUN zip -d ./logstash-core/lib/org/apache/logging/log4j/log4j-core/2.6.2/log4j-core-2.6.2.jar org/apache/logging/log4j/core/lookup/JndiLookup.class
USER logstash
RUN logstash-plugin remove x-pack && \
    sed --in-place '/xpack/d' /usr/share/logstash/config/logstash.yml;
